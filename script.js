/*
1. Опишіть своїми словами що таке Document Object Model (DOM)
  Це деревоподібна структура, яка представляє елементи документа HTML і дозволяє програмам 
  маніпулювати цими елементами та взаємодіяти з ними.
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
   Встановлюючи властивість innerHTML елемента, ми замінюємо вміст усередині 
   елемента новою розміткою HTML, а встановлюючи властивість innerHTML елемента
   ми додаємо сам текс в елемент
3. Як можна звернутися до елемента сторінки за допомогою JS? 
Який спосіб кращий?
   Ми можемо звернутися до елементів такими методами:
   - document.getElementById()
   - document.getElementsByClassName()
   - document.getElementsByTegName()
   - querySelector()
   - querySelectorAll()
   Кращим методом вважається querySelector(), так як getElementBy застарілий
*/

const paragraph = document.querySelectorAll("p");
paragraph.forEach((elem) => {
  elem.style.background = "#ff0000";
});

const idItem = document.querySelector("#optionsList");
console.log(idItem);

const parentElemIdItem = idItem.parentElement;
console.log(parentElemIdItem);

const childrenNodesIdItem = idItem.childNodes;
console.log(childrenNodesIdItem);

const pContent = document.querySelector("#testParagraph");
pContent.innerText = "This is a paragraph";
console.log(pContent);

const liItems = document.querySelectorAll(".main-header li");
liItems.forEach((elem) => {
  console.log(elem);
  elem.classList.add("nav-item");
});

const removeClass = document.querySelectorAll(".section-title");
removeClass.forEach((elem) => elem.classList.remove());
console.log(removeClass);
